#include <iostream>
#include "Pierre.h"

//Constructeur par défaut:va servir à remplir les cases vides du goban

Pierre::Pierre(){
    couleur=0;
    ch=0;
}

//Constructeur principal: construit la pierre avec ces coordonnées et sa couleur
Pierre::Pierre(unsigned int x_,unsigned int y_,int c)
{
    couleur=c;
    x=x_;
    y=y_;
}

//Constructeur par copie: va servir pour construire le backup si un joueur décide de revenir en arrière
Pierre::Pierre(const Pierre& p){
    couleur=p.couleur;
    x=p.x;
    y=p.y;
    ch=p.ch;

}


Pierre::~Pierre()
{
    //dtor
}


//Initialise l'attribut ch:si la pierre a un voisin de la même couleur, elle prend le même numéro de chaine et le transmet à 
//ces autres voisins potentielles de même couleur, sinon on lui attribue un entier
void Pierre::ajoutchaine(Pierre ***tab,int id,int taille){
    int coul=this->couleur;
    if(x>=1 && tab[x-1][y]->couleur==coul){
        this->ch=tab[x-1][y]->ch;
    }
    else if (x+1<taille && tab[x+1][y]->couleur==coul){
        this->ch=tab[x+1][y]->ch;
    }
    else if(y>=1 && tab[x][y-1]->couleur==coul){
        this->ch=tab[x][y-1]->ch;
    }
    else if (y+1<taille && tab[x][y+1]->couleur==coul){
        this->ch=tab[x][y+1]->ch;
    }
    else {
        this->ch=id;
    }

    if(x>=1 &&tab[x-1][y]->couleur==coul && this->ch!=tab[x-1][y]->ch){
        tab[x-1][y]->ch=this->ch;
        tab[x-1][y-1]->correcchaine(tab,taille);
    }

   if(y>=1 &&tab[x][y-1]->couleur==coul && this->ch!=tab[x][y-1]->ch){
        tab[x][y-1]->ch=this->ch;
        tab[x][y-1]->correcchaine(tab,taille);
    }

    if(y+1<taille && tab[x][y+1]->couleur==coul && this->ch!=tab[x][y+1]->ch){
        tab[x][y+1]->ch=this->ch;
        tab[x][y+1]->correcchaine(tab,taille);
    }

    if(x+1<taille && tab[x+1][y]->couleur==coul && this->ch!=tab[x+1][y]->ch){
        tab[x+1][y]->ch=this->ch;
        tab[x+1][y]->correcchaine(tab,taille);
    }


}

//Corrige l'attribut ch si necessaire pour que toutes les pierres voisines de même couleur aient le même numéro de chaine
void Pierre::correcchaine(Pierre ***tab,int taille){
    int coul=this->couleur;
    if(x>=1 && tab[x-1][y]->couleur==coul && this->ch!=tab[x-1][y]->ch){
        tab[x-1][y]->ch=this->ch;
        tab[x-1][y]->correcchaine(tab,taille);
    }

    if(x+1<taille && tab[x+1][y]->couleur==coul && this->ch!=tab[x+1][y]->ch){
        tab[x+1][y]->ch=this->ch;
        tab[x+1][y]->correcchaine(tab,taille);
    }

    if(y>=1 && tab[x][y-1]->couleur==coul && this->ch!=tab[x][y-1]->ch){
        tab[x][y-1]->ch=this->ch;
        tab[x][y-1]->correcchaine(tab,taille);
    }

    if(y+1<taille &&tab[x][y+1]->couleur==coul && this->ch!=tab[x][y+1]->ch){
        tab[x][y+1]->ch=this->ch;
        tab[x][y+1]->correcchaine(tab,taille);
    }

}


/*Renvoie 1 si la chaine à laquelle la pierre appartient a au moins une liberté, 0 si aucune liberté. 
On parcourt pour cela tout le goban, et on regarde si chaque pierre de cette chaine a une liberté ou non.
On fait un return immédiatement car on a l'information qu'il y a bien une liberté, inutile de continuer
*/ 
int Pierre::libchaine(Pierre ***tab,int taille){
    unsigned int i,j;
    for(i=0;i<taille;i++){
        for(j=0;j<taille;j++){
            if(tab[i][j]->ch==ch){
                
                if(i>0 && tab[i-1][j]->couleur==0){
                   
                    return 1;
                }
                if(i+1<taille && tab[i+1][j]->couleur==0){
                   
                    return 1;
                }
                if(j>0 && tab[i][j-1]->couleur==0){
              
                    return 1;
                }
                if(j+1<taille && tab[i][j+1]->couleur==0){
                    
                    return 1;
                }

            }
        }
    }
    return 0;
}

  
    
//Supprime la pierre, et supprime les pierres voisines de la même chaine
int Pierre::supprchaine(Pierre ***tab,int taille){
    unsigned int id=this->ch;
    unsigned int x2=x;
    unsigned int y2=y;
    *tab[x][y]=Pierre();
    int res=1;

    if(x2>=1 && id==tab[x2-1][y2]->ch)
        res=res+tab[x2-1][y2]->supprchaine(tab,taille);
    
    if(x2+1<taille && id==tab[x2+1][y2]->ch)
        res=res+tab[x2+1][y2]->supprchaine(tab,taille);
    
    if(y2>=1 && id==tab[x2][y2-1]->ch)
        res=res+tab[x2][y2-1]->supprchaine(tab,taille);

    if(y2+1<taille && id==tab[x2][y2+1]->ch)
        res=res+tab[x2][y2+1]->supprchaine(tab,taille);

    return res;
}

//renvoie l'attribut couleur
int Pierre::getcouleur(){
    return this->couleur;
}

int Pierre::getx(){
    return this->x;
}
int Pierre::gety(){
    return this->y;
}
int Pierre::getch(){
    return this->ch;
}
