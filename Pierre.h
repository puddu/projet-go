#ifndef PIERRE_H
#define PIERRE_H
#include <string>
//#include "Chaine.h"


class Pierre
{
    public:
        Pierre();
        Pierre(const Pierre&);
        Pierre(unsigned int x_,unsigned int y_,int c);

        virtual ~Pierre();
        void ajoutchaine(Pierre ***tab,int id,int taille);
        int libchaine(Pierre ***tab,int taille);
        void correcchaine(Pierre ***tab,int taille);
        int supprchaine(Pierre ***tab,int taille);


        int getcouleur();
        int getx();
        int gety();
        int getch();
    private:
        int couleur;
        unsigned int x;
        unsigned int y;
        unsigned int ch;

        
};
#endif // PIERRE_H
