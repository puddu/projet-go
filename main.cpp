#include <iostream>
#include <typeinfo>
#include <ctime>
#include <time.h>
#include "Pierre.h"

using namespace std;
//--std=c++11

//Affichage du goban: les cases vides sont représentées par des points
void affichage(Pierre ***tab,int t){
    unsigned int i,j;
    
 
    cout<<"    ";
    for(i=0;i<t;i++){
        if(i<10){
        cout<<i<<"  ";
        }
        else
            cout<<i<<" ";
    }
    cout<<endl;
    for(i=0;i<t;i++){
        for(j=0;j<t;j++){
            if(j==0){
                if(i<10)
               cout<<i<<"  ";
                else
                    cout<<i<<" ";
            }
            if(tab[i][j]->getcouleur()==1)
                std::cout<<" N ";
            if(tab[i][j]->getcouleur()==2)
                std::cout<<" B ";
            
            if(tab[i][j]->getcouleur()==0)
                std::cout<<" . ";
        }
    std::cout<<std::endl;
    }
}

/*Test pour vérifier la règle du suicide:
si la case n'a aucune liberté:on vérifie si on prive une chaine adverse de liberté et dans ce cas c'est bon,
sinon on ne peut pas mettre la pièce ici.

*/
int suicide(Pierre ***tab,int x,int y,int taille,int joueur,int chaine){
    Pierre *test=new Pierre(x,y,joueur);
    tab[x][y]=test;
    tab[x][y]->ajoutchaine(tab,chaine,taille);
    int res=1;
    if(test->libchaine(tab,taille)==0){
        if(x>0 && tab[x-1][y]->getcouleur()!=joueur && tab[x-1][y]->libchaine(tab,taille)==0){
            delete test;
            return 1;
        }
        if(x+1<taille && tab[x+1][y]->getcouleur()!=joueur && tab[x+1][y]->libchaine(tab,taille)==0){
            delete test;
            return 1;
        }
        if(y>0 &&  tab[x][y-1]->getcouleur()!=joueur && tab[x][y-1]->libchaine(tab,taille)==0){
            delete test;
            return 1;
        }
        if(y+1<taille && tab[x][y+1]->getcouleur()!=joueur && tab[x][y+1]->libchaine(tab,taille)==0){
            delete test;
            return 1;
        }
        delete test;
        return 0;
    }
    delete test;    
    return 1;
}

//Verifie la regle de ko: si le joueur a joué le même coup juste avant on lui interdit d'y rejouer
int ko(Pierre **tab,int (*hist)[3],int x,int y,int taille,int compteur){
    if(x==hist[compteur-2][1] && y==hist[compteur-2][2])
        return 0;
    else
        return 1;

}

//Supprime les pierres sans liberté, et renvoie le nombre de pierre capturées
int capture(Pierre ***tab,int t,int x,int y){
    int c=tab[x][y]->getcouleur();
    int res=0;
    if(x>0 && tab[x-1][y]->getcouleur()!=c && tab[x-1][y]->libchaine(tab,t)==0){
        
        res=res+tab[x-1][y]->supprchaine(tab,t);
    }
    if(x+1<t && tab[x+1][y]->getcouleur()!=c && tab[x+1][y]->libchaine(tab,t)==0){
           res=res+tab[x+1][y]->supprchaine(tab,t);
    }

    if(y>0 && tab[x][y-1]->getcouleur()!=c && tab[x][y-1]->libchaine(tab,t)==0){
        
        res=res+tab[x][y-1]->supprchaine(tab,t);
    }

    if(y+1<t && tab[x][y+1]->getcouleur()!=c && tab[x][y+1]->libchaine(tab,t)==0){
        res=res+tab[x][y+1]->supprchaine(tab,t);
    }

    return res;
}


//Demande si on affiche l'historique, et le fait dans le cas positif
void affichage_hist(int (*hist)[3],int taille){
    unsigned int i,j;
    char buf;
    cout<<"Souhaitez-vous afficher l'historique des coups?('o'pour oui, autre sinon)"<<endl;
    cin>>buf;
    if(buf=='o'){
        for(i=0;i<taille;i++){
            cout<<"Joueur "<<hist[i][0]<<" a joué en x="<<hist[i][1]<<" et y="<<hist[i][2]<<endl;
        }
    }
}

//Jeu en mode abandon: le jeu se termine quand les 2 joueurs abandonnent consécutivement
void mode_abandon(){
    int buffer;
   
    char buf,buf2,retour;
    std::cout<<"choississez la taille du goban"<<std::endl;
    std::cin>>buffer;
    unsigned int const taille=buffer;
    unsigned int i,j;

    //Initialisation du goban et du backup en cas de changement d'avis d'un joueur
    Pierre *vide=new Pierre();
    Pierre ***goban=new Pierre**[taille];
    for(i=0;i<taille;i++){
        goban[i]=new Pierre*[taille];
        for(j=0;j<taille;j++)
            goban[i][j]=vide;
    }

    Pierre ***backup=new Pierre**[taille];
    for(i=0;i<taille;i++){
        backup[i]=new Pierre*[taille];
    }

    //Initialisation de l'historique des coups
    int hist[taille*taille*2][3];
    
    affichage(goban,taille);

    

    unsigned int x,y;
    unsigned int compteur=0;
    int abandon=0,nb_abandon=0;
    int booleen;
    int joueur;
    
    //boucle de jeu avec comme condition d'arret:les 2 joueurs ont choisi l'abandon
    while(abandon!=2){
        buf='n';
        
        joueur=compteur%2+1;
        if(joueur==1)
            std::cout<<"Joueur noir:"<<endl;
        else
            std::cout<<"Joueur blanc:"<<endl;

       
        std::cout<<"Voulez-vous abandonnez?(o pour oui,autre sinon)"<<endl;
        std::cin>>buf;
        //on incremente abandon si oui, et le tour passe à l'adversaire
        if(buf=='o'){
            nb_abandon++;
            compteur++;
            abandon++;
            continue; 
        }
        else abandon=0;

        //On redemande les coordonnées où placer une pierre tant que x et y ne sont pas valides
        while(1){
            std::cout<<"Tapez les coordonnées de votre pierre"<<endl;
            std::cin>>x;
            std::cin>>y;
            
            if(x<0 || x>=taille || y<0 || y>=taille ){
                cout<<"Coordonnées incorrectes"<<endl;
                x=0;
                y=0;
                continue;
            }
            if(goban[x][y]->getcouleur()!=0){
                cout<<"Case déjà occupée"<<endl;
                x=0;
                y=0;
                continue;

            }
            if(suicide(goban,x,y,taille,joueur,compteur+1)==0){
                cout<<"Suicide"<<endl;
                continue;
            }
            if(ko(*goban,hist,x,y,taille,compteur)==0){
                cout<<"Regle de ko"<<endl;
                continue;
            }
            break;
    }

        
        /*La pierre peut maintenant être rajouté: on l'ajoute au goban, on initialise son numéro de chaine 
        en ajustant eventuellement celui d'autres pierres
        puis on essaye de capturer des pierres si certaines peuvent l'être
        */
        Pierre *nouveau=new Pierre(x,y,joueur);
        goban[x][y]=nouveau;
        goban[x][y]->ajoutchaine(goban,compteur+1,taille);
        capture(goban,taille,x,y);

        affichage(goban,taille);
        cout<<"Souhaitez-vous revenir en arrière?('o' pour oui, autre sinon)"<<endl;
        cin>>retour;
        if(retour=='o'){
            
            for(i=0;i<taille;i++)
            for(j=0;j<taille;j++){
                Pierre *nouv=new Pierre(*backup[i][j]);
                goban[i][j]=nouv;
            }
            affichage(goban,taille);
            continue;
        }       

        hist[compteur-nb_abandon][0]=joueur;
        hist[compteur-nb_abandon][1]=x;
        hist[compteur-nb_abandon][2]=y;
        compteur++;
        
        for(i=0;i<taille;i++)
            for(j=0;j<taille;j++){
                Pierre *nouv=new Pierre(*goban[i][j]);
                backup[i][j]=nouv;
            }

    }

    //Décompte des pierres pour annoncer qui a gagné

    int nb_1=0,nb_2=0;
    for(i=0;i<taille;i++){
        for(j=0;j<taille;j++){
            if(goban[i][j]->getcouleur()==1)
                nb_1++;
            if(goban[i][j]->getcouleur()==2)
                nb_2++;
        }
    }

    cout<<"Le joueur 1 a "<<nb_1<<" pierres"<<endl;
    cout<<"Le joueur 2 a "<<nb_2<<" pierres"<<endl;
    if(nb_1>nb_2){
        cout<<"Le joueur 1 a gagné"<<endl;
    }
    else if(nb_1<nb_2){
        cout<<"Le joueur 2 a gagné"<<endl;
    }
    else if(nb_1==nb_2){
        cout<<"Egalité"<<endl;;
    }

    affichage_hist(hist,compteur-nb_abandon);
     for(i=0;i<taille;i++)
        for(j=0;j<taille;j++)
            delete goban[x][y];
    delete goban;

}

void mode_1capture(){
    int buffer;

    char buf,buf2,retour;
    std::cout<<"choississez la taille du goban"<<std::endl;
    std::cin>>buffer;
    unsigned int const taille=buffer;
    unsigned int i,j;

    Pierre *vide=new Pierre();
    Pierre ***goban=new Pierre**[taille];
    for(i=0;i<taille;i++){
        goban[i]=new Pierre*[taille];
        for(j=0;j<taille;j++)
            goban[i][j]=vide;
    }


    int hist[taille*taille*2][3];
    
    affichage(goban,taille);

    Pierre ***backup=new Pierre**[taille];
    for(i=0;i<taille;i++){
        backup[i]=new Pierre*[taille];
    }

    unsigned int x,y;
    unsigned int compteur=0;
    int abandon=0;
    int booleen;
    int joueur;
    int res=0;
    while(res==0 && compteur!=taille*taille){
        buf='n';
        
        joueur=compteur%2+1;
        if(joueur==1)
            std::cout<<"Joueur noir:"<<endl;
        else
            std::cout<<"Joueur blanc:"<<endl;
      
        while(1){
            std::cout<<"Tapez les coordonnées de votre pierre"<<endl;
            std::cin>>x;
            std::cin>>y;
            
            if(x<0 || x>=taille || y<0 || y>=taille ){
                cout<<"Coordonnées incorrectes"<<endl;
                x=0;
                y=0;
                continue;
            }
            if(goban[x][y]->getcouleur()!=0){
                cout<<"Case déjà occupée"<<endl;
                x=0;
                y=0;
                continue;

            }
            if(suicide(goban,x,y,taille,joueur,compteur+1)==0){
                cout<<"Suicide"<<endl;
                continue;
            }
            if(ko(*goban,hist,x,y,taille,compteur)==0){
                cout<<"Regle de ko"<<endl;
                continue;
            }
            break;
    }

        
        
        Pierre *nouveau=new Pierre(x,y,joueur);
        goban[x][y]=nouveau;
        goban[x][y]->ajoutchaine(goban,compteur+1,taille);
        res=capture(goban,taille,x,y);
        cout<<"res="<<res<<endl;
        affichage(goban,taille);
        cout<<"Souhaitez-vous revenir en arrière?('o' pour oui, autre sinon)"<<endl;
        cin>>retour;
        if(retour=='o'){
    
            for(i=0;i<taille;i++)
            for(j=0;j<taille;j++){
                Pierre *nouv=new Pierre(*backup[i][j]);
                goban[i][j]=nouv;
            }
            affichage(goban,taille);
            continue;
         }
        
        hist[compteur][0]=joueur;
        hist[compteur][1]=x;
        hist[compteur][2]=y;
        compteur++;

        for(i=0;i<taille;i++)
            for(j=0;j<taille;j++){
                Pierre *nouv=new Pierre(*goban[i][j]);
                backup[i][j]=nouv;
            }

    }

    if(res!=0){
        cout<<"Le joueur "<<joueur<<" a gagné!"<<endl;
    }
    else
        cout<<"Match nul:le tableau est plein et aucun joueur n'a capturé de pierre"<<endl;


    affichage_hist(hist,compteur);
    for(i=0;i<taille;i++)
        for(j=0;j<taille;j++)
            delete goban[x][y];
    delete goban;

}

void mode_5capture(){
    int buffer;

    char buf,buf2,retour;
    std::cout<<"choississez la taille du goban"<<std::endl;
    std::cin>>buffer;
    unsigned int const taille=buffer;
    unsigned int i,j;

    Pierre *vide=new Pierre();
    Pierre ***goban=new Pierre**[taille];
    for(i=0;i<taille;i++){
        goban[i]=new Pierre*[taille];
        for(j=0;j<taille;j++)
            goban[i][j]=vide;
    }


    int hist[taille*taille*2][3];
    
    affichage(goban,taille);

    Pierre ***backup=new Pierre**[taille];
    for(i=0;i<taille;i++){
        backup[i]=new Pierre*[taille];
    }

    unsigned int x,y;
    unsigned int compteur=0;
    int abandon=0;
    int booleen;
    int joueur;
    int capture_1=0,capture_2=0;
    while(capture_1<5 && capture_2<5 && compteur!=taille*taille){
        buf='n';
        
        joueur=compteur%2+1;
        if(joueur==1)
            std::cout<<"Joueur noir:"<<endl;
        else
            std::cout<<"Joueur blanc:"<<endl;
      
        while(1){
            std::cout<<"Tapez les coordonnées de votre pierre"<<endl;
            std::cin>>x;
            std::cin>>y;
            
            if(x<0 || x>=taille || y<0 || y>=taille ){
                cout<<"Coordonnées incorrectes"<<endl;
                x=0;
                y=0;
                continue;
            }
            if(goban[x][y]->getcouleur()!=0){
                cout<<"Case déjà occupée"<<endl;
                x=0;
                y=0;
                continue;

            }
            if(suicide(goban,x,y,taille,joueur,compteur+1)==0){
                cout<<"Suicide"<<endl;
                continue;
            }
            if(ko(*goban,hist,x,y,taille,compteur)==0){
                cout<<"Regle de ko"<<endl;
                continue;
            }
            break;
    }

        
        
        Pierre *nouveau=new Pierre(x,y,joueur);
        goban[x][y]=nouveau;
        goban[x][y]->ajoutchaine(goban,compteur+1,taille);

        if(joueur==1)
            capture_1=capture_1+capture(goban,taille,x,y);
        else
            capture_2=capture_2+capture(goban,taille,x,y);

      
        affichage(goban,taille);
        cout<<"Souhaitez-vous revenir en arrière?('o' pour oui, autre sinon)"<<endl;
        cin>>retour;
        if(retour=='o'){
            
            for(i=0;i<taille;i++)
            for(j=0;j<taille;j++){
                Pierre *nouv=new Pierre(*backup[i][j]);
                goban[i][j]=nouv;
            }
            affichage(goban,taille);
            continue;
         }        

        hist[compteur][0]=joueur;
        hist[compteur][1]=x;
        hist[compteur][2]=y;
        compteur++;

        for(i=0;i<taille;i++)
            for(j=0;j<taille;j++){
                Pierre *nouv=new Pierre(*goban[i][j]);
                backup[i][j]=nouv;
            }

    }

    if(capture_1>=5){
        cout<<"Le joueur 1 a gagné!"<<endl;
    }
    else if(capture_2>=5){
        cout<<"Le joueur 2 a gagné!"<<endl;
    }
    else
        cout<<"Match nul:le tableau est plein et aucun joueur n'a capturé de pierre"<<endl;


    affichage_hist(hist,compteur);
    for(i=0;i<taille;i++)
        for(j=0;j<taille;j++)
            delete goban[x][y];
    
    delete goban;

}



int main()
{
    //message d'annonce du jeu
    int buffer;
    cout<<"choississez le mode de jeu: Tapez 1 pour mode avec abandon, 2 pour victoire à la première capture, 3 pour victoire au 5 premières pierres capturés"<<endl;
    cin>>buffer;
    if(buffer==1)
        mode_abandon();
    if(buffer==2)
        mode_1capture();
    if(buffer==3)   
        mode_5capture();

    


    return 0;
}
